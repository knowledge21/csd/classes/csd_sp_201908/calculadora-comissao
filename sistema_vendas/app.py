from flask import Flask
from .calculadora_comissao import CalculadoraComissao

app = Flask(__name__)

@app.route("/comissao/<float:venda>")
def calcula_comissao(venda):
    comissao = CalculadoraComissao.calcular(venda)
    return "%s" % comissao
