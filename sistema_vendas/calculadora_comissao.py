import math
class CalculadoraComissao(object):
    LIMITE_COMISSAO = 10000
    COMISSAO_MENOR_10K = 0.05
    COMISSAO_MAIOR_10K = 0.06

    @classmethod
    def calcular(cls, valor_venda):
        if valor_venda > cls.LIMITE_COMISSAO:
            comissao = valor_venda * cls.COMISSAO_MAIOR_10K
        else:
            comissao = valor_venda * cls.COMISSAO_MENOR_10K
        return math.floor(comissao*100)/100